@extends('layout.index')

@section('content')
<div class="container employee-container">
    <div class="section-employee">
        <button type="button" class="btn btn-primary add-emp-button" data-toggle="modal" data-target="#employeeModal">
            Add New Employee
        </button>

        <div class="employee-table">
            <table class="table" style="margin-top: 2%">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">First Name</th>
                        <th scope="col">Last Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Pin Code</th>
                        <th scope="col">Address</th>
                        <th scope="col">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($employeeList as $employee)
                    <tr>
                        <td>{{ $employee -> id }}</td>
                        <td>{{ $employee -> first_name }}</td>
                        <td>{{ $employee -> last_name }}</td>
                        <td>{{ $employee -> email }}</td>
                        <td>{{ $employee -> pin_code }}</td>
                        <td>{{ $employee -> address }}</td>

                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

{{-- Model --}}

<div class="modal fade" id="employeeModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Employee</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ url('/new/employee') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputEmail4">First Name</label>
                            <input type="text" class="form-control" name="firstname" id="">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Last Name</label>
                            <input type="text" name="lastname" class="form-control" id="">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputEmail4">Email</label>
                            <input type="email" name="email" class="form-control" id="">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Pin Code</label>
                            <input type="text" name="pincode" class="form-control" id="">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md--12">
                            <label for="inputAddress">Address</label>
                            <input type="text" name="address" class="form-control" id="inputAddress" placeholder="">
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Add</button>
            </div>
            </form>
        </div>
    </div>
</div>

@endsection