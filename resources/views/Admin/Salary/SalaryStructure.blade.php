@extends('layout.index')

@section('content')
<div class="container salary-container">
    <div class="section-salary">
        <div class="salary" align="end">
            <button type="button" class="btn btn-primary add-emp-button" data-toggle="modal" data-target="#salaryModal">
                Add Salary
            </button>
        </div>

        {{-- Table --}}
        <div class="salary-tables">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Emplyoee Id</th>
                        <th scope="col">Salary Description</th>
                        <th scope="col">Basic Pay</th>
                        <th scope="col">HRA</th>
                        <th scope="col">CCA</th>
                        <th scope="col">SA</th>
                        <th scope="col">MA</th>
                        <th scope="col">CA</th>
                        <th scope="col">PI</th>
                        <th scope="col">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($salaryList as $emp)
                    <tr>
                        <th scope="row">{{ $emp->id }}</th>
                        <td>{{ $emp->salaryStructure->first_name }} {{ $emp->salaryStructure->last_name}}</td>
                        <td>{{ $emp -> description }}</td>
                        <td>{{ $emp -> basic_pay }}</td>
                        <td>{{ $emp -> house_rent }}</td>
                        <td>{{ $emp -> city_compensatory }}</td>
                        <td>{{ $emp -> special_allowances }}</td>
                        <td>{{ $emp -> medical_allowances }}</td>
                        <td>{{ $emp -> conveyance_allowances }}</td>
                        <td>{{ $emp -> performance_incentives }}</td>
                        <td>
                            <a class="btn btn-primary editSalaries" id="editSalary" data-id="{{  $emp->id }}" href=""
                                data-toggle="modal">Edit</a>
                            <a class="btn btn-danger" href="">Delete</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    {{-- Models --}}
    <div class="modal fade" id="salaryModal">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Salary</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('newSalary') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="form-group  col-lg-6">
                                <label class="form-control-label" for="inputBasicFirstName">Employee Name</label>
                                <select name="empId" id="" class="form-control">
                                    <option value="">--</option>
                                    @foreach($employeeList as $emp)
                                    <option value="{{$emp->id }}">{{$emp->first_name}} {{ $emp->last_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-lg-6">
                                <label class="form-control-label" for="inputBasicLastName">Salary Description</label>
                                <!-- <textarea name="salaryDescription" class="form-control "></textarea> -->
                                <input type="text" class="form-control" name="salaryDescription">
                            </div>
                            <div class="form-group col-lg-6">
                                <label class="form-control-label" for="inputBasicLastName">Basic Pay</label>
                                <input type="number" class="form-control" name="basicSalary">
                            </div>
                            <div class="form-group col-lg-6">
                                <label class="form-control-label" for="inputBasicLastName">House Rent Allownces</label>
                                <input type="number" class="form-control" name="houseRentAllownces">
                            </div>
                            <div class="form-group col-lg-6">
                                <label class="form-control-label" for="inputBasicLastName">City Compensatory
                                    Allownces</label>
                                <input type="number" class="form-control" name="cityCompensatoryAllownces">
                            </div>
                            <div class="form-group  col-lg-6">
                                <label class="form-control-label" for="inputBasicLastName">Special Allownces</label>
                                <input type="number" class="form-control " name="specialAllownces">
                            </div>
                            <div class="form-group col-lg-6">
                                <label class="form-control-label " for="inputBasicLastName">Medical Allownces</label>
                                <input type="number" class="form-control " name="medicalAllownces">
                            </div>
                            <div class="form-group col-lg-6">
                                <label class="form-control-label " for="inputBasicLastName">Conveyance Allownces</label>
                                <input type="number" class="form-control " name="conveyanceAllownces">
                            </div>
                            <div class="form-group col-lg-6">
                                <label class="form-control-label " for="inputBasicLastName">Performance
                                    Incentives</label>
                                <input type="number" class="form-control " name="performanceIncentives">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Add</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    {{-- Edit Models --}}
    <div class="modal fade editSalary" id="editSalary">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editTitle">Edit Salary</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" method="POST">
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <div class="row">
                            <div>
                                <input type="hidden" class="form-control salaries">
                            </div>
                            <div class="form-group  col-lg-6">
                                <label class="form-control-label" for="inputBasicFirstName">Employee Name</label>
                                <select name="empId" id="empId" class="form-control empId">
                                    <option value="">--</option>
                                    @foreach($employeeList as $emp)
                                    <option value="{{$emp->id }}">{{$emp->first_name}} {{ $emp->last_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-lg-6">
                                <label class="form-control-label" for="inputBasicLastName">Salary Description</label>
                                <!-- <textarea name="salaryDescription" class="form-control "></textarea> -->
                                <input type="text" class="form-control salaryDescription" name="salaryDescription">
                            </div>
                            <div class="form-group col-lg-6">
                                <label class="form-control-label" for="inputBasicLastName">Basic Pay</label>
                                <input type="number" class="form-control basicSalary" name="basicSalary">
                            </div>
                            <div class="form-group col-lg-6">
                                <label class="form-control-label" for="inputBasicLastName">House Rent Allownces</label>
                                <input type="number" class="form-control houseRentAllownces" name="houseRentAllownces">
                            </div>
                            <div class="form-group col-lg-6">
                                <label class="form-control-label" for="inputBasicLastName">City Compensatory
                                    Allownces</label>
                                <input type="number" class="form-control cityCompensatoryAllownces"
                                    name="cityCompensatoryAllownces">
                            </div>
                            <div class="form-group  col-lg-6">
                                <label class="form-control-label" for="inputBasicLastName">Special Allownces</label>
                                <input type="number" class="form-control specialAllownces" name="specialAllownces">
                            </div>
                            <div class="form-group col-lg-6">
                                <label class="form-control-label " for="inputBasicLastName">Medical Allownces</label>
                                <input type="number" class="form-control medicalAllownces" name="medicalAllownces">
                            </div>
                            <div class="form-group col-lg-6">
                                <label class="form-control-label " for="inputBasicLastName">Conveyance Allownces</label>
                                <input type="number" class="form-control conveyanceAllownces"
                                    name="conveyanceAllownces">
                            </div>
                            <div class="form-group col-lg-6">
                                <label class="form-control-label " for="inputBasicLastName">Performance
                                    Incentives</label>
                                <input type="number" class="form-control performanceIncentives"
                                    name="performanceIncentives">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <a class="btn btn-primary updateSalary">Update</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        // Edit Salary
        $(document).on('click', '.editSalaries', function(){
            var _id = $(this).data('id');
            console.log('Salary Id', _id);

            $.ajax({
                type: "GET",
                url: "/salary/edit/"+_id,
                success: function(data){
                    
                    $('.editSalary').modal('show');
                    $('.salaries').val(data.id);
                    $(".empId").val(data.emp_id);
                    $(".salaryDescription").val(data.description); 
                    $(".basicSalary").val(data.basic_pay);
                    $(".houseRentAllownces").val(data.house_rent);
                    $(".cityCompensatoryAllownces").val(data.city_compensatory);
                    $(".specialAllownces").val(data.performance_incentives);
                    $(".medicalAllownces").val(data.medical_allowances);
                    $(".conveyanceAllownces").val(data.conveyance_allowances);
                    $(".performanceIncentives").val(data.performance_incentives);
                }
            })
        });

        // Update Salary
        $('.updateSalary').on('click', function(){
            console.log('HIII');
            var id = $(".salaries").val();
            console.log('Update Id', id);
            $.ajax({
                url:'/salary/update/'+id,
                type: 'POST',
                cache: false,
                data: {
                    _token:'{{ csrf_token() }}',
                    type: 3,
                    empId: $(".empId").val(),
                    salaryDescription: $(".salaryDescription").val(),
                    basicSalary:  $(".basicSalary").val(),
                    houseRentAllownces:  $(".houseRentAllownces").val(),
                    cityCompensatoryAllownces:  $(".houseRentAllownces").val(),
                    specialAllownces:  $(".specialAllownces").val(),
                    medicalAllownces:  $(".medicalAllownces").val(),
                    conveyanceAllownces:  $(".conveyanceAllownces").val(),
                    performanceIncentives: $(".performanceIncentives").val()
                },
                success: function(dataResult){
                    console.log('Updated Result', dataResult);
                    dataResult = JSON.parse(dataResult);
                    if(dataResult){
                        window.location = '/salary';
                    }
                    else{
                        alert('Internal server error');
                    }
                }
            })
        })
    })

</script>
@endsection