@extends('layout.index')

@section('content')
<div class="container management-container">
    <div class="section-salary-management">
        <h1>Salary Management</h1>
        <div class="card">
            <div class="salary-management">
                <form action="" method="">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="form-group  col-xs-12 col-md-6">
                            <label class="form-control-label label" style="padding: 10px"
                                for="inputBasicLastName">Employee Name</label>
                            <select name="empId" value="{{ request()-> empId}}" id="" style="margin: 15px 10px 10px 2px"
                                class="form-control empId">
                                <option value="-">-</option>
                                @foreach($employeeList as $emp)
                                <option value="{{ $emp -> id }}">{{$emp->first_name}} {{ $emp->last_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group  col-xs-12 col-md-6">
                            <label class="form-control-label label" style="padding: 10px"
                                for="inputBasicLastName">Select month</label>
                            <input type="month" value="{{ request()-> month }}" style="margin: 15px 0px 10px -2px"
                                class="form-control month" name="month">
                        </div>
                    </div>

                    <div align="center" style="padding: 10px; margin: 10px">
                        <a class="btn btn-primary salaryManagement" style="color:white">Get Salary</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="card" id="card">
        <div class="salaryStatus" align="end">

        </div>
    </div>
</div>


<style>
     
            body{
                font-family: "Roboto", sans-serif;
                font-size: 18px; 
            }

            .employee-details h4{
                padding: 10px
                
            }

            .employee-details .content{
                padding-left: 30px;
                font-family: "Roboto", sans-serif;
                font-size: 18px; 
            }

            .employee-details .content h5{
                font-family: Arial, Helvetica, sans-serif;
                font-size: 18px; 
                font-weight: bold;
            }

            .employee-details .employee-data{
                font-family: Arial, Helvetica, sans-serif;
                font-size: 18px;
            }
        
</style>
<script>
    $(document).ready(function(){
        $(document).on('click','.salaryManagement', function(){

            var id = $('.empId').val();
            console.log('Employee Id', id);
            var month = $('.month').val();
            console.log('Month', month);

            $.ajax({
                url: '/salary/status/'+id,
                type: 'post',
                cache: false,
                data: {
                    _token:'{{ csrf_token() }}',
                    type: 3,
                    empId: id,
                    month: month
                },
                success: function(statusData){
                    console.log('EmpId', statusData)
                    if(statusData.status == 0){
                        $('.salaryStatus').append("<a class='btn btn-warning Status' id='status' data-id="+id+" data-toggle='model'>Confirm Salary</a>").css("padding", "20px");
                    }
                    else{
                        $('.salaryStatus').append("<button type='button' class='btn btn-success' disabled>Paid</buttton>").css("padding", "20px");
                    }
                }
            });

            // Calculated Salary
            $.ajax({
                url:'/salary/management/'+id,
                type: 'post',
                cache: false,
                data:{
                    _token:'{{ csrf_token() }}',
                    type: 3,
                    empId: id,
                    month: month
                },
                success: function(data){
                    var salary = 
                    "<div class='employee-details'>\
                        <h4>Employee Details</h4>\
                        <br>\
                        <div class='row'>\
                            <div class='col-sm-6 col-md-6 col-lg-6 col-xs-6 content'>\
                                <p>Name</p>\
                                <p>Email</p>\
                                <p>Address</p>\
                                <p>Pin Code</p>\
                            </div>\
                            <div class='col-sm-4 col-md-4 col-lg-4 col-xs-4 employee-data'>\
                                <p>"+data.employeeDetails.first_name+" "+data.employeeDetails.last_name+"</p>\
                                <p>"+data.employeeDetails.email+"</p>\
                                <p>"+data.employeeDetails.address+"</p>\
                                <p>"+data.employeeDetails.pin_code+"</p>\
                            </div>\
                        </div>\
                    </div>\
                    <hr>\
                    <div class='employee-details'>\
                        <h4>Salary Head</h4>\
                        <br>\
                        <div class='row'>\
                            <div class='col-sm-6 col-md-6 col-lg-6 col-xs-6 content'>\
                                <p> Basic Pay </p>\
                                <p> House Rent Allowances(HRA) </p>\
                                <p> City Compensatory Allowance(CCA) </p>\
                                <p> Special Allowances(SA) </p>\
                                <p> Medical Allowances(MA) </p>\
                                <p> Conveyance Allowances(CA) </p>\
                                <p> Performance Incentives(PI) </p>\
                            </div>\
                            <div class='col-sm-4 col-md-4 col-lg-4 col-xs-4 employee-data'>\
                                <p>"+data.salarySructure.basic_pay+"</p>\
                                <p>"+data.salarySructure.house_rent+"</p>\
                                <p>"+data.salarySructure.city_compensatory+"</p>\
                                <p>"+data.salarySructure.special_allowances+"</p>\
                                <p>"+data.salarySructure.medical_allowances+"</p>\
                                <p>"+data.salarySructure.conveyance_allowances+"</p>\
                                <p>"+data.salarySructure.performance_incentives+"</p>\
                            </div>\
                        </div>\
                    </div>\
                    <hr>\
                    <div class='employee-details'>\
                        <h4> Attendance </h4>\
                        <div class='row'>\
                            <div class='col-sm-6 col-md-6 col-lg-6 col-xs-6 content'>\
                                <p> Working Days </p>\
                                <p> Leave Days </p>\
                            </div>\
                            <div class='col-sm-4 col-md-4 col-lg-4 col-xs-4 employee-data'>\
                                <p>"+data.totalAttendance+"</p>\
                                <p>"+data.leaves+"</p>\
                            </div>\
                        </div>\
                    </div>\
                    <hr>\
                    <div class='employee-details'>\
                        <div class='row'>\
                            <div class='col-sm-6 col-md-6 col-lg-6 col-xs-6 content'>\
                                <h5> Total Monthly Gross Salary </h2>\
                                <hr>\
                                <h5> Annual Gross Salary </h2>\
                            </div>\
                            <div class='col-sm-4 col-md-4 col-lg-4 col-xs-4 employee-data'>\
                                <p>"+data.inHandSalary+"</p>\
                                <hr>\
                                <p>"+data.annualSalary+"</p>\
                            </div>\
                        </div>\
                    </div>"

                    $('#card').append(salary);
                }
            })
        });

        // Update Status
        $(document).on('click','.Status',function(){
            var empId = $('.empId').val();
            console.log('status Id', empId);

            $.ajax({
                url: '/salary/status/update/'+empId,
                type: 'post',
                cache: false,
                data: {
                    _token:'{{ csrf_token() }}',
                    type: 3,
                    empId: $('.empId').val(),
                    date:  $('.month').val(),
                    status: 1
                },
                success: function(dataResult){
                    console.log('Updated Result', dataResult);
                    dataResult = JSON.parse(dataResult);
                    if(dataResult.statusCode){
                        window.location = '/management';
                    }
                    else{
                        alert('Internal Server Error');
                    }
                }
            })
        });
    })
</script>
@endsection