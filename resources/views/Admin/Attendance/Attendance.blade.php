@extends('layout.index')

@section('content')
<div class="container attendance-container">
    <div class="section-attendance">
        <div class="new-attendance" align="end">
            <button class="btn btn-primary" data-toggle="modal" data-target="#attendanceModal">New
                Attendence</button>
        </div>

        {{-- Tables --}}
        <div class="attendance-tables">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Employee Id</th>
                        <th scope="col">Date</th>
                        <th scope="col">Punch In</th>
                        <th scope="col">Punch Out</th>
                        <th scope="col">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($attendanceList as $attendances)
                    <tr>
                        <td>{{ $attendances -> id }}</td>
                        <td>{{ $attendances -> attendance ->first_name }}</td>
                        <td>{{ $attendances-> date }}</td>
                        <td>{{ $attendances -> punch_in}}</td>
                        <td>{{ $attendances -> punch_out }}</td>
                        <td>
                            <a class="btn btn-primary editAttendance" id="editAttendance"
                                data-id="{{  $attendances->id }}" href="" data-toggle="modal">Edit</a>
                            <a class="btn btn-danger" href="">Delete</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div align="end">
            {{ $attendanceList->links() }}
        </div>
    </div>

    {{-- Models --}}
    <div class="modal fade categories" id="attendanceModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        &times;
                    </button>
                    <h4 class="modal-title">Add Attendance</h4>
                </div>
                <div class="modal-body">
                    <div class="nav-tabs-horizontal nav-tabs-animate" data-plugin="tabs">
                        <ul class="nav nav-tabs nav-tabs-line" role="tablist">
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" data-toggle="tab" href="#single_attendance"
                                    aria-controls="single_attendance" role="tab">Single</a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" data-toggle="tab" href="#multiple_attendance"
                                    aria-controls="multiple_attendance" role="tab">Multiple </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane animation-fade" id="single_attendance" role="tabpanel">
                                <form action="{{ route('singleAttendance')}}" method="POST">
                                    {{ csrf_field() }}
                                    <div class="row">
                                        <div class="form-group  col-xs-12 col-md-6">
                                            <label class="form-control-label" for="inputBasicFirstName">Employee
                                                Name</label>
                                            <select name="emp_id" id="" class="form-control">
                                                <option value="">--</option>
                                                @foreach($employeeList as $emp)
                                                <option value="{{$emp->id }}">{{$emp->first_name}}
                                                    {{ $emp->last_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group  col-xs-12 col-md-6">
                                            <label class="form-control-label" for="inputBasicLastName">Date</label>
                                            <input type="date" class="form-control date" id="date" name="date">
                                        </div>
                                        <div class="form-group col-xs-12 col-md-6">
                                            <label class="form-control-label" for="inputBasicLastName">Punch
                                                In</label>
                                            <input type="time" class="form-control punch_in" name="punchin">
                                        </div>
                                        <div class="form-group  col-xs-12 col-md-6 pass1">
                                            <label class="form-control-label password" for="inputBasicLastName">Punch
                                                Out</label>
                                            <input type="time" class="form-control punch_out" name="punchout">
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Add</button>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane animation-fade" id="multiple_attendance" role="tabpanel">
                            <form action="{{ route('multipleAttendance') }}" method="POST" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="" style="padding: 2%">
                                        <h2>Generate the CSV</h2>
                                    <a href="{{ route('fileDownload')}}">Download the File</a>
                                    </div>
                                    <div class="">
                                        <input type="file" class="form-contril" name="file" id="">
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Add</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Edit Model --}}
    <div class="modal fade editAttendances" id="editAttendances">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editTitle">Edit Attendance</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" method="POST">
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="">
                                <input type="hidden" class="attendance" name="">
                            </div>
                            <div class="form-group  col-xs-12 col-md-6">
                                <label class="form-control-label" for="inputBasicFirstName">Employee Name</label>
                                <input type="text" class="form-control employeeId" name="employeeid">
                            </div>
                            <div class="form-group  col-xs-12 col-md-6">
                                <label class="form-control-label" for="inputBasicLastName">Date</label>
                                <input type="date" class="form-control date" name="date">
                            </div>
                            <div class="form-group col-xs-12 col-md-6">
                                <label class="form-control-label" for="inputBasicLastName">Punch In</label>
                                <input type="time" class="form-control punchIn" name="punchin">
                            </div>
                            <div class="form-group  col-xs-12 col-md-6 pass1">
                                <label class="form-control-label password" for="inputBasicLastName">Punch
                                    Out</label>
                                <input type="time" class="form-control punchOut" name="punchout">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <a class="btn btn-primary edit" style="color: white">Update</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

{{-- Scripts --}}
<script>
    $(document).ready(function(){
        $(document).on('click','.editAttendance',function(){
            var attendance_id = $(this).data('id');
            console.log('Attendance Id', attendance_id);
            $.ajax({
                type: "GET",
                url: "/attendance/edit/"+attendance_id,
                success: function(data){
                    console.log('data',data);
                    $('#editTitle').html('Edit Attendance');
                    $('.editAttendances').modal('show');
                    $('.attendance').val(data.id);
                    $(".employeeId").val(data.employee_id);
                    $(".date").val(data.date); 
                    $(".punchIn").val(data.punch_in);
                    $(".punchOut").val(data.punch_out);
                }
            })
        })
    })
</script>
@endsection