<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\EmployeeAttendance;

use Maatwebsite\Excel\Facades\Excel;

use App\Imports\BulkImport;

class EmployeeAttendanceController extends Controller
{
    //

    public function list(){
      
        $attendanceList = EmployeeAttendance::with('attendance')->paginate(5);
        return view('/Admin/Attendance/Attendance', compact('attendanceList'));
    }

    public function singleAttendance(Request $request){
        $this -> validate($request,[
            "emp_id"=>'required|integer',
            "date"=> 'required',
            "punchin"=>'required',
            "punchout"=>'required'
        ]);

        $attendance = new EmployeeAttendance;
        $attendance->emp_id = $request -> emp_id;
        $attendance->date = $request -> date;
        $attendance->punch_in = $request -> punchin;
        $attendance->punch_out = $request -> punchout;
        $attendance -> save();
        return redirect(route('attendanceList'));    
    }

    public function multipleAttendance(){

        Excel::import(new BulkImport,request()->file('file'));

        return back();
    }

    public function fileDownload(){
        $pathToFile = public_path('employee_attendances.csv');
        $name = 'AttendanceSheet.csv';
        $headers = [
            'Content-type'=> 'text/csv'
        ];
        return response()->download($pathToFile, $name, $headers);
    }

    public function edit($id){
        $employeeAttencdanceList = EmployeeAttendance::with('attendance')->get()->find($id);
           return response()->json($employeeAttencdanceList);
    }

    public function update(Request $request, $id)
    {
        //
        $this -> validate($request,[
            "emp_id"=>'required|integer',
            "date"=> 'required',
            "punchin"=>'required',
            "punchout"=>'required'
        ]);

        $updateAttendance = EmployeeAttendance::find($id);    
        $updateAttendance->emp_id = $request-> emp_id;
        $updateAttendance->date = $request -> date;
        $updateAttendance->punch_in = $request -> punchin;
        $updateAttendance->punch_out = $request -> punchout;
        $updateAttendance -> save();
        return json_encode(array('statusCode'=>200));
    }
}
