<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\SalaryStructure;

class SalaryStructureController extends Controller
{
    //
    public function list(){
        $salaryList = SalaryStructure::with('salaryStructure')->get();
        return view('/Admin/Salary/SalaryStructure',compact('salaryList'));
    }

    //
    public function add(Request $request){
        $this -> validate($request,[
            "empId"=>'required|integer',
            "salaryDescription"=>'required',
            "basicSalary"=>'required',
            "houseRentAllownces"=>'required',
            "cityCompensatoryAllownces"=>'required',
            "specialAllownces"=>'required',
            "medicalAllownces"=>'required',
            "conveyanceAllownces"=>'required',
            "performanceIncentives"=>'required',
        ]);

        $emp_salary = new SalaryStructure;
        $emp_salary->emp_id = $request -> empId;
        $emp_salary ->description = $request -> salaryDescription;
        $emp_salary ->basic_pay = $request -> basicSalary;
        $emp_salary ->house_rent = $request -> houseRentAllownces;
        $emp_salary ->city_compensatory = $request -> cityCompensatoryAllownces;
        $emp_salary ->special_allowances = $request -> specialAllownces;
        $emp_salary ->medical_allowances = $request -> medicalAllownces;
        $emp_salary ->conveyance_allowances = $request -> conveyanceAllownces;
        $emp_salary ->performance_incentives = $request -> performanceIncentives;
        $emp_salary -> save();
        return redirect(route('salaryStructureList'));
    }

    public function edit($id){
        $editSalary = SalaryStructure::with('salaryStructure')->get()->find($id);
        return response()->json($editSalary);
    }

    public function update(Request $request,$id){

        $this -> validate($request,[
            "empId"=>'required|integer',
            "salaryDescription"=>'required',
            "basicSalary"=>'required|integer',
            "houseRentAllownces"=>'required|integer',
            "cityCompensatoryAllownces"=>'required|integer',
            "specialAllownces"=>'required|integer',
            "medicalAllownces"=>'required|integer',
            "conveyanceAllownces"=>'required|integer',
            "performanceIncentives"=>'required|integer',
        ]);

        $updateSalary = SalaryStructure::find($id);
        $updateSalary->emp_id = $request -> empId;
        $updateSalary ->description = $request -> salaryDescription;
        $updateSalary ->basic_pay = $request -> basicSalary;
        $updateSalary ->house_rent = $request -> houseRentAllownces;
        $updateSalary ->city_compensatory = $request -> cityCompensatoryAllownces;
        $updateSalary ->special_allowances = $request -> specialAllownces;
        $updateSalary ->medical_allowances = $request -> medicalAllownces;
        $updateSalary ->conveyance_allowances = $request -> conveyanceAllownces;
        $updateSalary ->performance_incentives = $request -> performanceIncentives;
        $updateSalary -> save();
        return json_encode(array('statusCode'=>200));
    }
}
