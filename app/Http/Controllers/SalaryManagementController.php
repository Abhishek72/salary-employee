<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\SalaryStructure;

use App\Models\SalaryStatus;

use App\Models\Employee;

use App\Models\EmployeeAttendance;

use Carbon\Carbon;

class SalaryManagementController extends Controller
{
    //

    public function management(){

        return view('/Admin/Salary/SalaryManagement');
    }

    public function salaryCalcualte(Request $request,$id){
        $empId = $request->input('empId');
            $month = $request->input('month');

            $employeeDetails = Employee::where('id',$empId)->get()->first();

            $monthInDate =Carbon::create(explode("-",$month)[0],explode("-",$month)[1],1);
            $daysInMonth = $monthInDate->daysInMonth;
            
            $startDateOfMonth = $monthInDate->startOfMonth()->toDateString();
            $endDateOfMonth = $monthInDate->endOfMonth()->toDateString();

            $totalAttendance = EmployeeAttendance::where('emp_id',$empId)->where('date','>=',$startDateOfMonth)->where('date','<=',$endDateOfMonth)->get()->count();
            $leaves = $daysInMonth - $totalAttendance;
            // return $leaves;

            // Fetch the salary structure
            $salarySructure = SalaryStructure::where('emp_id',$empId)->get()->first();
            
            // 
            $totalSalary = $salarySructure->getTotal();

            // calculate per_day_salary
            $perDaySalary = intval($totalSalary / $daysInMonth);
            
            // calculate in hand salary
            $inHandSalary = intval($perDaySalary * $totalAttendance);


            // Annual salary
            $annualSalary = $inHandSalary * 12;
            
            // calculate deduction factor
            $deductionFactor = intval((100*$inHandSalary)/$totalSalary);
            
            return response()->json([
                'totalAttendance'=> $totalAttendance,
                'employeeDetails'=>$employeeDetails,
                'leaves'=>$leaves,
                'salarySructure'=>$salarySructure,
                'totalSalary'=>$totalSalary,
                'perDaySalary'=>$perDaySalary,
                'inHandSalary'=>$inHandSalary,
                'deductionFactor'=>$deductionFactor,
                'annualSalary'=>$annualSalary
                ],
                200);
    }

    public function salaryStatus(Request $request,$id){
        $empId = $request-> input('empId');
        $month = $request -> input('month');

        return response()->json($this->common($empId,$month),200);
    }

    public function statusUpdate(Request $request,$id){
        $updateStatus = new SalaryStatus;
        $updateStatus->emp_id = $request -> empId;
        $updateStatus->date = $request -> date;
        $updateStatus->status = $request -> status;
        $updateStatus->save();

        return json_encode(array('statusCode'=>200));
    }


    public function common($empId,$month){
        
        $salaryStaus = SalaryStatus::where('emp_id',$empId)->get();
        
        if (count($salaryStaus) == 0) {
            
            $status = new SalaryStatus;
                $status->emp_id = $empId;
                
                $status->status = 0;
                return $status;    
        }
        else{
            return $salaryStaus;
        }
    }
    
}
