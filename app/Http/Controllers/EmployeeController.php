<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Employee;

class EmployeeController extends Controller
{
    //
    public function list(){

        
        return view('/Admin/Employee/Employee');
    }

    public function add(Request $request){

       $this -> validate($request, [
           'firstname' => 'required',
           'lastname' => 'required',
           'email' => 'required',
           'pincode' => 'required',
           'address' => 'required'
       ]); 

        $employee = new Employee;
        $employee->first_name = $request -> firstname;
        $employee->last_name = $request -> lastname;
        $employee->email = $request -> email;
        $employee->pin_code = $request -> pincode;
        $employee->address = $request -> address;
        $employee->save();
        return redirect(route('employeeList'));
    }

    public function edit(){
        
    }

    public function update(){
        
    }

    public function delete(){
        
    }
}
