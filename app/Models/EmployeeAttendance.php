<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmployeeAttendance extends Model
{
    //
    protected $table = 'employee_attendance';

    protected $fillable = [
        'emp_id', 'date', 'punch_in','punch_out'
    ];

    public function attendance()
    {
        return $this->belongsTo('App\Models\Employee', 'emp_id', 'id');
    }
}
