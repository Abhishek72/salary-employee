<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalaryStatus extends Model
{
    //
    protected $table = 'salary_status';

    protected $fillable = [
        'emp_id', 'date', 'status'
    ];
}
