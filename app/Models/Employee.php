<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    //
    protected $table = 'employee';

    protected $fillable = [
        'first_name', 'last_name', 'email','address', 'pin_code'
    ];

    // Relations
    public function attendance()
    {
        return $this->hasMany('App\Models\EmployeeAttendance');
    }

    public function salaryStructure()
    {
        return $this -> hasMany('App\Models\SalaryStructure');    
    }
}
