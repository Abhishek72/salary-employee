<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalaryStructure extends Model
{
    //
    protected $table = 'salary_structure';

    protected $fillable = [
        'emp_id', 'description', 'basic_pay','house_rent','pin_code','city_compensatory','special_allowances','medical_allowances','conveyance_allowances','performance_incentives'
    ];

    public function salaryStructure()
    {
        return $this->belongsTo('App\Models\Employee', 'emp_id', 'id');
    }

    public function getTotal(){
        return $this->basic_pay + $this->house_rent_allowances + $this->city_compensatory_allowance + $this->special_allowances + $this->medical_allowances + $this->conveyance_allowances + $this->performance_incentives;
    }
}
