<?php

namespace App\Exports;

use App\EmployeeAttendance;
use Maatwebsite\Excel\Concerns\FromCollection;

class UsersExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return EmployeeAttendance::query()->get(['employee_id','date','punch_in','punch_out']);
    }
}
