<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Bulk extends Model
{
    protected $table = 'employee_attendance';
    protected $fillable = [
        'emp_id', 'date','punch_in', 'punch_out'
    ];
}