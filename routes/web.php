<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


// Employee

    // Get Employee List
    Route::get('/employee', 'EmployeeController@list')->name('employeeList');
    // Add New Employee
Route::post('/new/employee', 'EmployeeController@add')->name('newEmployee');
    //Edit Employee
Route::get('/employee/edit/{id}', 'EmployeeController@edit')->name('editEmployee');
        //Update Employee  
Route::get('/employee/update/{id}', 'EmployeeController@update')->name('updateEmployee');
    // Delete Employee
Route::get('/employee/delete/{id}', 'EmployeeController@delete')->name('deleteEmployee');    

// Attendance

    //Get Attendance List
Route::get('/attendance','EmployeeAttendanceController@list')->name('attendanceList');

    //Single Attendance
Route::post('/single/attendance', 'EmployeeAttendanceController@singleAttendance')->name('singleAttendance');
    //Multiple Attendance
Route::post('/multiple/attendance', 'EmployeeAttendanceController@multipleAttendance')->name('multipleAttendance');
        //File Download
Route::get('/attendance/file', 'EmployeeAttendanceController@fileDownload')->name('fileDownload');
        //Edit Attendance
Route::get('/attendance/edit/{id}', 'EmployeeAttendanceController@edit')->name('editAttendance');
        //Update Attendance
Route::post('/attendance/update/{id}', 'EmployeeAttendanceController@update')->name('updateAttendance');
        // Delete Attendance
Route::get('/attendance/delete/{id}', 'EmployeeAttendanceController@delete')->name('deleteAttendance');

// Salary Structure

    // Get Salary Structure
Route::get('/salary', 'SalaryStructureController@list')->name('salaryStructureList');
    //Add Salary
Route::post('/employee/salary', 'SalaryStructureController@add')->name('newSalary');
    //Edit Salary
Route::get('/salary/edit/{id}', 'SalaryStructureController@edit')->name('editDalary');
    //Update Salary
Route::post('/salary/update/{id}', 'SalaryStructureController@update')->name('updateSalary');
    //Delete Salary
Route::get('/salary/delete/{id}', 'SalaryStructureController@delete')->name('deleteSalary');


// Salary Management
Route::get('/management', 'SalaryManagementController@management')->name('salaryManagement');

Route::post('/salary/management/{id}', 'SalaryManagementController@salaryCalcualte');

Route::post('/salary/status/{id}', 'SalaryManagementController@salaryStatus');

Route::post('/salary/status/update/{id}', 'SalaryManagementController@statusUpdate');